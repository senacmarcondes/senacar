﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Senacar.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Descricao : ContentPage
	{

        private const int VIDROS = 545;
        private const int TRAVAS = 250;
        private const int AR = 480;
        private const int CAMERA = 180;
        private const int CAMBIO = 450;
        private const int SUSPENSAO = 380;
        private const int FREIOS = 245;
        


        public string TextoVidros
        {
            get
            {
                return string.Format("Vidros Eletricos - R$ {0}", VIDROS);
            }
        }

        public string TextoTravas
        {
            get
            {
                return string.Format("Travas Eletricas - R$ {0}", TRAVAS);
            }
        }

        public string TextoAr
        {
            get
            {
                return string.Format("Ar Condicionado - R$ {0}", AR);
            }
        }

        public string TextoCamera
        {
            get
            {
                return string.Format("Camera de Ré - R$ {0}", CAMERA);
            }
        }

        public string TextoCambio
        {
            get
            {
                return string.Format("Cambio - R$ {0}", CAMBIO);
            }
        }

        public string TextoSuspensao
        {
            get
            {
                return string.Format("Suspensão - R$ {0}", SUSPENSAO);
            }
        }

        public string TextoFreios
        {
            get
            {
                return string.Format("Freios - R$ {0}", FREIOS);
            }
        }


        public string ValorTotal
        {
            get
            {
                return string.Format("Total - R$ {0}", Veiculos.Preco
                    + (IncluiVidro ? VIDROS : 0)
                    + (IncluiTravas ? TRAVAS : 0)
                    + (IncluiAr ? AR : 0)
                    + (IncluiCamera ? CAMERA : 0)
                    + (IncluiCambio ? CAMBIO : 0)
                    + (IncluiSuspensao ? SUSPENSAO : 0)
                    + (IncluiFreios ? FREIOS : 0));
            }
        }












        bool incluiVidro;

        public bool IncluiVidro
        {
            get
            {
                return incluiVidro;
            }
            set
            {
                incluiVidro = value;
                if (incluiVidro)
                    DisplayAlert("Vidro", "Ativo", "Ok");
                else
                    DisplayAlert("Vidro", "Inativo", "Ok");
                OnPropertyChanged(nameof(ValorTotal));
            }
        }

        bool incluiTravas;

        public bool IncluiTravas
        {
            get
            {
                return incluiTravas;
            }
            set
            {
                incluiTravas = value;
                if (incluiTravas)
                    DisplayAlert("Travas", "Ativo", "Ok");
                else
                    DisplayAlert("Travas", "Inativo", "Ok");
                OnPropertyChanged(nameof(ValorTotal));
            }
        }

        bool incluiAr;

        public bool IncluiAr
        {
            get
            {
                return incluiAr;
            }
            set
            {
                incluiAr = value;
                if (incluiAr)
                    DisplayAlert("Ar Condicionado", "Ativo", "Ok");
                else
                    DisplayAlert("Ar condicionado", "Inativo", "Ok");
                OnPropertyChanged(nameof(ValorTotal));
            }
        }

        bool incluiCamera;

        public bool IncluiCamera
        {
            get
            {
                return incluiCamera;
            }
            set
            {
                incluiCamera = value;
                if (incluiCamera)
                    DisplayAlert("Camera de Ré", "Ativo", "Ok");
                else
                    DisplayAlert("Camera de Ré", "Inativo", "Ok");
                OnPropertyChanged(nameof(ValorTotal));
            }
        }

        bool incluiCambio;

        public bool IncluiCambio
        {
            get
            {
                return incluiCambio;
            }
            set
            {
                incluiCambio = value;
                if (incluiCambio)
                    DisplayAlert("Cambio", "Ativo", "Ok");
                else
                    DisplayAlert("Cambio", "Inativo", "Ok");
                OnPropertyChanged(nameof(ValorTotal));
            }
        }

        bool incluiSuspensao;

        public bool IncluiSuspensao
        {
            get
            {
                return incluiSuspensao;
            }
            set
            {
                incluiSuspensao = value;
                if (incluiSuspensao)
                    DisplayAlert("Suspensão", "Ativo", "Ok");
                else
                    DisplayAlert("Suspensão", "Inativo", "Ok");
                OnPropertyChanged(nameof(ValorTotal));
            }
        }

        bool incluiFreios;

        public bool IncluiFreios
        {
            get
            {
                return incluiFreios;
            }
            set
            {
                incluiFreios = value;
                if (incluiFreios)
                    DisplayAlert("Freios", "Ativo", "Ok");
                else
                    DisplayAlert("Freios", "Inativo", "Ok");
                OnPropertyChanged(nameof(ValorTotal));
            }
        }

        Veiculos Veiculos { get; set; }


        public Descricao (Veiculos veiculos)
		{
			InitializeComponent ();
            this.Title = veiculos.Nome;
            this.Title = veiculos.Nome2;
            this.Veiculos = veiculos;
            this.BindingContext = this;
		}

        private void ButtonProximo_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Agendamento(Veiculos));
        }

    }
}