﻿using Senacar.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Senacar
{
        public class Veiculos
        {
            public string Nome { get; set; }
            public string Nome2 { get; set; }
            public float Preco { get; set; }
        }

    public partial class ListaView : ContentPage
    {

        public List<Veiculos> Veiculos { get; set; }

      
       

        public ListaView()
        {
                     InitializeComponent();

            this.Veiculos = new List<Veiculos>
            {
                new Veiculos {Nome= "Ferrari", Nome2= "Bullet", Preco= 900000},
                new Veiculos {Nome= "Ferrari", Nome2= "Infernus", Preco= 1000000},
                new Veiculos {Nome= "Chevrolet", Nome2= "Greenwood", Preco= 25000},
                new Veiculos {Nome= "Ford", Nome2= "Bandito", Preco= 60000},
                new Veiculos {Nome= "BMW", Nome2= "SUperGT", Preco= 800000}
            };

            this.BindingContext = this;

            ListViewCarros.ItemsSource = this.Veiculos;

            

        }

        private void ListViewVeiculos_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var Veiculo = (Veiculos)e.Item;
            Navigation.PushAsync(new Descricao(Veiculo));

        }
    }
}
