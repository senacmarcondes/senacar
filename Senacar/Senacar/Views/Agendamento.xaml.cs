﻿using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Senacar.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Senacar.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Agendamento : ContentPage
    {

        public Veiculos Veiculos { get; set; }


        public Agendamento(Veiculos veiculos)
        {
            InitializeComponent();
            this.Title = veiculos.Nome;
            this.Title = veiculos.Nome2;
        }

        private void ButtonAgendamento_Clicked(object sender, EventArgs e)
        {

            Permissao();
        }

        public async void Permissao()
        {
            try
            {
                var status = await
                CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);
                if (status != PermissionStatus.Granted)
                {
                    if (await

                    CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Storage))
                    {
                        await DisplayAlert("Permissão", "Precisamos da sua permissão paraarmazenar dados no dispositivo.", "OK");

                    }
                    var results = await CrossPermissions.Current.RequestPermissionsAsync(new[]

                    { Permission.Storage });

                    status = results[Permission.Storage];
                }
                if (status == PermissionStatus.Granted)
                {
                    await DisplayAlert("Sucesso!", "Prossiga com a operação", "OK");
                    FazerAgendamento();
                }
                else if (status != PermissionStatus.Unknown)
                {
                    await DisplayAlert("Atenção", "Para utilizar os serviços de persistência dedados, aceite a permissão solicitada pelo dispositivo.", "OK");
                }
            }
            catch (Exception ex)
            {
                //await DisplayAlert("Erro", "Erro no processo " + ex.Message, "OK");
            }
        }

            public void FazerAgendamento()
            {
                using (var conexao = DependencyService
                        .Get<ISQLite>()
                        .Conexao())
                {
                    AgendamentoDAO dao = new AgendamentoDAO(conexao);
                    dao.Salvar(new Models.
                        Agendamento("Ferrari Bullet", "2313541", "Luan@email.com"));

                    DisplayAlert("Sucesso!", "Agendamento feito com sucesso", "Ok");
                }

            }

        }
    }
